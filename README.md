# plot_ISPC.jl

NeuroAnalyzer plugin: plot ISPC.

This software is licensed under [The 2-Clause BSD License](LICENSE).

## Usage

```julia
p = plot_ispc(eeg, eeg, ch1="Fp1", ch2="Fp2", ep1=4, ep2=4)
plot_save(p, file_name="ispc.png")
```

![](ispc.png)